# Temp Dev Envs POC

Prototype code for creation and managment of dynamic environments.

## Project structure

 * [jenkins](./jenkins) - sample jenkins pipelines
 * [scripts](./scripts) - utility scripts used in jenkins pipelines
 * [terraform](./terraform)
   * [base-infra](./terraform/base-infra) - infrastructure components for managing dev environments
   * [infra](./terraform/infra) - dev infrastructure components
