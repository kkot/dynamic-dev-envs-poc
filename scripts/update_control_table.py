import argparse
import logging

logging.basicConfig(level=logging.INFO)

def deactivate_deployment(env_id):
    # set is_active_deployment to False for given env_id in environments control table
    pass

def insert_environment_details(env_id):
    # create record in environments control table for given env_id (is_active_deployment set as True, deployment_timestamp set as datetime.now())
    # if record for given env_id already exist call update_environment_details()
    pass

def update_environment_details(env_id):
    # set deployment_timestamp to datetime.now()) for given env_id in environments control table
    pass

def is_environment_deployed():
    # check if record for env_id exist in control table and is_active_deployment is set to True
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--action', choices=['create', 'deactivate'])
    parser.add_argument('--env-id', type=str)

    args = parser.parse_args()

    if args.action == 'deactivate':
        deactivate_deployment(args.env_id)
    else:
        if is_environment_deployed(args.env_id):
            update_environment_details(args.env_id)
        else:
            insert_environment_details(args.env_id)
