locals {
  common_tags = {
    Project     = "myproject"
    EnvironmentId = var.env_id
    EnvironmentName = "dev"
  }
}

locals {
    resource_prefix = "${local.common_tags["EnvironmentName"]}-${local.common_tags["EnvironmentId"]}-${local.common_tags["Project"]}"
}