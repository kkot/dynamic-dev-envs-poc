provider "aws" {
  region = "eu-west-2"
}

terraform {
  backend "s3" {
    bucket = "temp-devenvs-config-bucket"
    # pass backend s3 key parameter as --backend-config="key=state/myproject/$env_id/"
    region = "eu-west-2"
  }
}