resource "aws_s3_bucket" "some_bucket" {
  bucket = "${local.resource_prefix}-somebucket"
  acl    = "private"
}