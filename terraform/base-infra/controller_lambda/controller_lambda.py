import boto3
import gitlab

def get_active_envs():
    # get list of active (is_active_deployment set to True) environments from env control table
    pass

def branch_exist(env_id):
    # check if branch exist for given env_id (dev-$env_id branch)
    pass

def is_environment_stale(env_id):
    # check if environment was created more then 7 days ago
    pass

def trigger_environment_deletion(env_id):
    # trigger jenkins job responsible for environment deletion passing env_id as parameter 
    pass

def handler(event, context):
    active_envs = get_active_envs()

    for env in active_envs:
        env_id = env.get('env_id')
        if not branch_exist(env_id) or is_environment_stale(env_id):
            trigger_environment_deletion(env_id)