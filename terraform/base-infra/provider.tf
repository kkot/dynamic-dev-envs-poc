provider "aws" {
  region = "eu-west-2"
}

terraform {
  backend "s3" {
    bucket = "temp-devenvs-config-bucket"
    key    = "state/base/state"
    region = "eu-west-2"
  }
}