resource "aws_dynamodb_table" "control_table" {
  name           = "${local.resource_prefix}-control-table"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "EnvironmentId"

  attribute {
    name = "EnvironmentId"
    type = "S"
  }

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-control-table"
  })
}

data "template_file" "controller_lambda_role" {
  template = file("${path.module}/lambda_policies/assume_role_policy.json")
}

data "template_file" "controller_lambda_execution_policy" {
  template = file("${path.module}/lambda_policies/role_policy.json")
}

resource "aws_iam_role" "controller_lambda_role" {
  name = "${local.resource_prefix}-controller-lambda-role"

  assume_role_policy = data.template_file.controller_lambda_role.rendered
}

resource "aws_iam_role_policy" "controller_lambda_execution_policy" {
  name   = "${local.resource_prefix}-controller-lambda-role-policy"
  role   = aws_iam_role.controller_lambda_role.id
  policy = data.template_file.controller_lambda_execution_policy.rendered
}

data "archive_file" "controller_lambda_zip" {
  type        = "zip"
  source_dir  = "controller_lambda"
  output_path = "controller_lambda.zip"
}

resource "aws_lambda_function" "controller_lambda" {
  function_name    = "${local.resource_prefix}-controller-lambda"
  filename         = "controller_lambda.zip"
  source_code_hash = data.archive_file.controller_lambda_zip.output_base64sha256
  handler          = "controller_lambda.handler"
  role             = aws_iam_role.controller_lambda_role.arn
  runtime          = "python3.8"
  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-controller-lambda"
  })

  environment {
    variables = {
      CONTROL_TABLE_NAME = aws_dynamodb_table.control_table.name
    }
  }
}

resource "aws_cloudwatch_event_rule" "controller_lambda_trigger" {
  name                = "${local.resource_prefix}-controller-lambda-trigger"
  schedule_expression = "rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "controller_lambda_event" {
  rule      = aws_cloudwatch_event_rule.controller_lambda_trigger.name
  target_id = "lambda"
  arn       = aws_lambda_function.controller_lambda.arn
}

resource "aws_lambda_permission" "controller_lambda_trigger_premissions" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.controller_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.controller_lambda_trigger.arn
}
