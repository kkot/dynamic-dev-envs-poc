locals {
  common_tags = {
    Project     = "dynamic-dev-environments"
    Environment = "manage"
  }
}

locals {
    resource_prefix = "${local.common_tags["Environment"]}-${local.common_tags["Project"]}"
}